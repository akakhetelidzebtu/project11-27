package com.btu.project1127

class MessageModel(
    var message:String? = null,
    var display_name:String? = null,
    var user_id:String? = null,
    var time_stamp:Long? = null
)